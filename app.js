// [SECTION] Dependencies and Modules
	const exp = require("express");
	const mongoose = require("mongoose");
// [SECTION] Server Setup
	const app = exp();

	const port = 4000;
// [SECTION] DataBase Connection
	mongoose.connect('mongodb+srv://senandoiii:admin123@cluster0.n9zyt.mongodb.net/toDo176?retryWrites=true&w=majority',{

		useNewUrlParser: true,
		useUnifiedTopology:true
	});
	
	//create a notification if the connection to the db is a success or a failure:

	let db = mongoose.connection;
	//let's add an on() connection from our mongoose connection to show if the connection has succeeded or failed in both the terminal and in the browser for our client

	db.on('error',console.error.bind(console,"Connection Error"));

	db.once('open', ()=> console.log("Connected to MongoDB"));

	app.use(exp.json());

	//Schema
	//before we can create documents from our API to save into our darabase, we first have to determing the structure of the documents to be writter in our database. This is to ensure the consistency of our documents and avoid futur errors.

	//Schema acts as a blueprint for our data/document.

	//Schema() contructor from mongoose to create a new chema object

	const taskSchema = new mongoose.Schema({

		/*
			Define the fields for our documents.
			We will also be able to determing the appropriate data type of the values.
		*/
		name: String,
		status: String
	});

	//Mongoose Model
	/*
		Models are used to connect your api to the corresponding collection in your database. It is a representation of your collection.

		Models uses schemas to create object that correspond to the schema. By default, when creating the collection from your model, the collection name is pluralized.

		mongoose.model(<nameofCollectionInAtlas>,<schemaToFollow>)
	*/

	const Task = mongoose.model("task", taskSchema);

	//post route to create a new task

	app.post('/tasks', (req, res) => {
		//When creating a new post/put or any route that requires data from the client, first console log your req.body or any poart of the request that contains the data.
		// console.log(req.body);

		//Creating a new task document by using the constructor of our Task model. This constructor should follow the schema of the model
		let newTask = new Task({

			name: req.body.name,
			status: req.body.status
		})

		//.save() method from an object created by a model.
		//.save() method will allow us to save our document by connecting to our collection via our model

		//save() has 2 approached: 1. we can add an anonymous function to handle the creadted document or error.
		//2. we can add .then() chain which will allow us to handle errors and created documents in separate functions

		// newTask.save((error,savedTask)=>{

		// 	if(error){
		// 		res.send("Document creation Failed.");
		// 	} else {
		// 		res.send(savedTask)
		// 	}
		// })

		newTask.save()
		.then(result => res.send({message: "Document Creation Succesful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));
	});


	const sampleSchema = new mongoose.Schema({
		name: String,
		isActive: Boolean 
	})


	const Sample = mongoose.model("samples", sampleSchema);

	app.post('/samples', (req, res) => {
		let newSample = new Sample({

			name: req.body.name,
			isActive: req.body.isActive
		})

		newSample.save((savedSample,error) =>{
			if (error){;
				res.send(error)
			} else {
				res.send(newSample);
			}
		})

	})

	app.get('/tasks', (req,res)=>{

		//TO query using mongoose, first access the model of the collection you want to manipulate

		Task.find({},)
		.then(result => res.send(result))
		.catch(error => res.send(error));
	})

	app.get('/samples', (req, res)=>{

		Sample.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));
	})

	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	});

	const User = new mongoose.model("user", userSchema)

	app.post('/users', (req,res) =>{

		let newUser = new User({
			username: req.body.username,
			password: req.body.password
		})
		newUser.save()
		.then(result => res.send(result))
		.catch(error => res.send(error));
	})

	app.get('/users', (req, res) =>{

		User.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error));
	})
// [SECTION] Entry Point Response
	app.listen(port, () => console.log(`Server running at port: ${port}`));