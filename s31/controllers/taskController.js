//Controllers contains the functions and business logic of our Express JS Application

const Task = require("../models/task");

//[Controllers]
//

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	});
};


module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			return false
		} else {
			return task
		}
	});
};

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((fulfilled, rejected) => {
		if (fulfilled) {
			return 'The task has been successfully Removed';
		} else {
			return 'Failed to remove Task';
		}
	});
};

module.exports.taskCompleted = (taskId) => {

	return Task.findById(taskId).then((found, error) =>{ 
			if (found) {
				// console.log(found);
				found.status = 'Completed';
				return found.save().then((update, error) =>{
					if (update) {
						return 'Task successfully modified';
					} else {
						return 'Task Failed to Update'
					}
				});
			} else {
				return 'Error! No Task Found' ;
			};
	});
};

module.exports.taskPending = (userInput) => {
	return Task.findById(userInput).then((result,err)=>{
		if (result) {
			result.status = 'Pending';
			return result.save().then((taskUpdated, error)=>{
				if (taskUpdated) {
					return `Task ${taskUpdated.name} Successfully Executed`;
				} else {
					return 'Error when saving task updates';
				}
			});
		} else {
			return 'Something Went Wrong'
		}
	});
};