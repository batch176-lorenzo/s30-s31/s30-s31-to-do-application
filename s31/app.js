const exp = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute")

const app = exp();
const port = 4000;

app.use(exp.json());

mongoose.connect('mongodb+srv://senandoiii:admin123@cluster0.n9zyt.mongodb.net/toDo176?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology:true
});

let db = mongoose.connection;

	db.on('error',console.error.bind(console, "Connection Error"));
	db.once('open', () => console.log("Connected to MongoDB"));

app.use("/tasks", taskRoute);	

app.listen(port, () => console.log(`Welcome to Server Port: ${port}`));

