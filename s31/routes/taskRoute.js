//Contains all the endpoints for out application
//We seperate the routes such that "app.js" only contains information on the server

const express = require("express");

//Create a router instance that functions as a middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();


const taskController = require("../controllers/taskController")

//Routes
//Route for getting all the tasks

router.get("/", (req, res) =>{
	taskController.getAllTasks().then(resultfromController => res.send(resultfromController))
});

//Route for creating a task 

router.post("/", (req, res) =>{
	taskController.createTask(req.body).then(resultfromController => res.send(resultfromController));
});

router.delete("/:task", (req,res) =>{
	console.log(req.params.task);

	// res.send('Hello From Delete');
	taskController.deleteTask(req.params.task).then(result => res.send(result));
});

router.put("/:task", (req,res) => {
	taskController.taskCompleted(req.params.task).then(result => res.send(result));
});

router.put("/:task/pending", (req,res) =>{
	taskController.taskPending(req.params.task).then(result => res.send(result));
});
//Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;